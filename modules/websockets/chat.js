function createServer () {
	var clients = [];

	
	var ws = new WebSocket({
		server: server,
		path: "/websockets/chat"
	});

	var temp = database.collection('temp');
	ws.on('connection', function (connection) {
		var index = clients.push(connection) - 1;
		temp.update({service:'ws_chat'},{$push:{clients:{user:'',connectionId:index,serverPid:process.pid}}});
		var pinger = setInterval(function(){
		for(var i=0;i<ws.clients.length;i++){
			ws.clients[i].send(JSON.stringify({type:'ping'}));		
		}
		},45000);
		connection.on('message', function (msg) {
			if (msg == '') {
				connection.send('{ "type" : "nodata"}');
			}
			try {
				var jsonmsg = JSON.parse(msg);
			} catch (e) {
				connection.send("SEND JSON PLZ");
				return true;
			}
			if (jsonmsg.type == "pid") {
				connection.send(JSON.stringify({type:'pid',pid:process.pid}))
			}
			if (jsonmsg.type == 'helo') {
				connection.send(JSON.stringify({type:'helo-reply'}))	
			}
			if (jsonmsg.type== 'list'){
				connection.send(JSON.stringify({type:'online',data:clients.length}));			
			}
			if (jsonmsg.type == 'details'){
				connection.send(JSON.stringify({type:'startchat',value:false,message:'Username exists'}))			
			}
			if(jsonmsg.type == 'msg'){
				mqsend.send({destination:'/queue/ws-chat',body:jsonmsg.msg});		
			}

		});
		connection.on('close',function(connection){
			clients.splice(index,1);
		
		});
	});
}
createServer();
