
/*
 * GET home page.
 */

exports.get = function(req, res){
	var sessId=req.cookies.sessionId;
	database.collection('users').update({sessionId:sessId},{$pull:{sessionId:sessId}});
	res.cookie('sessionId', '', { maxAge: 90, httpOnly: true });
	res.redirect('/');
};
