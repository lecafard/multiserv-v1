
/*
 * GET home page.
 */

exports.get = function(req, res){
    functions.sessions.validate(database.collection('users'),req.cookies.sessionId,function(ans){
		if(ans==false){
			res.render(config.site.template+'/login', { title: config.site.brand+' - Login' , brand:config.site.brand,loggedIn:ans,messages:true});
		}
		else{
			res.redirect("/");
		}
    });
  
};
exports.post = function(req,res){
	var loggedIn = false;
	var email = req.body.email;
	var pass = req.body.pass;
	hash = crypto.createHash('sha1').update(pass).digest("hex");	
	functions.sessions.authenticate(database.collection('users'),email,pass,function(sessId){
		if(sessId==false){
			res.render(config.site.template+'/login', { title: config.site.brand+' - Login' , brand:config.site.brand,loggedIn:false,messages:false});
		}
		else{
			res.cookie('sessionId', sessId, { httpOnly: true });
			setTimeout(res.redirect('/'),500);		
		}
	});
}
