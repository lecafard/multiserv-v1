
/*
 * GET home page.
 */
var checkr = require('validator').check,
    sanitize = require('validator').sanitize;
var validator = new checkr()
validator.error = function(msg) {
    return false;
}	
exports.get = function(req, res){
  functions.sessions.validate(database.collection('users'),req.cookies.sessionId,function(loggedIn){
	  if(req.query.token){
		token =req.query.token
		helpline = database.collection('helpline');
		helpline.find({token:token}).toArray(function(err,db){
			res.render(config.site.template+"/helpline/idexists",{ title: 'Helpline',brand:config.site.brand,message:db[0].message,token:token,loggedIn:loggedIn});
		});
	  }
	  else{
	  	res.render(config.site.template+'/helpline/default', { title: 'Helpline',brand:config.site.brand,warnings:"",inputm:"",loggedIn:loggedIn});
}	
});

};
exports.post = function(req,res){
	email = req.body.email;
	message = req.body.message;
	message = sanitize(message).xss();
	response = '<div class="alert alert-danger"><b>Please fix these things up.</b>';
	loggedIn =false;
	compare = response;
	if(req.query.token){
		token =req.query.token
		helpline = database.collection('helpline');
		helpline.find({token:token}).toArray(function(err,db){
			
		});
  		res.render(config.site.template+"/helpline/idexists",{ title: 'Helpline',brand:config.site.brand,warnings:"",token:token});
  	}
	if(validator.check(email).isEmail()==false){
		response+="<br><p>Please Enter a valid email address.</p>";
	}
	if(response==compare){
		helpid = functions.random(50);
		helpline = database.collection('helpline');
		helpline.insert({email:email,message:[{user:email,message:message}],token:helpid})
		res.redirect("/helpline?token="+helpid);
	}
	else{
		res.render(config.site.template+"/helpline/default",{ title: 'Helpline',brand:config.site.brand,warnings:response+"</div>",inputm:message,loggedIn:loggedIn});
	}
}
