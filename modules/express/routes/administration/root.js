
/*
 * GET home page.
 */
root = {}
root.get = function(req, res){
	functions.sessions.validate(database.collection('users'),req.cookies.sessionId,function(ans){
		if(ans.usertype=='admin'){
			res.render(config.site.template+'/administration/root', { title: 'Admin',brand:config.site.brand,loggedIn:ans,unAuthorised:0});
		}
		else{
			res.render(config.site.template+'/index', { title: 'GET OUT',brand:config.site.brand,loggedIn:ans,unAuthorised:1});	
		}
	});
};
app.get('/administration',root.get);
