
/*
 * GET home page.
 */
users = {}
users.get = function(req, res){
	functions.sessions.validate(database.collection('users'),req.cookies.sessionId,function(ans){
		if(ans.usertype=='admin'){
			database.collection('users').find().toArray(function(err,all){
				res.render(config.site.template+'/administration/users', { title: 'Admin',brand:config.site.brand,loggedIn:ans,unAuthorised:0,allUsers:all});
			});
		}
		else{
			res.render(config.site.template+'/index', { title: 'GET OUT',brand:config.site.brand,loggedIn:ans,unAuthorised:1});	
		}
	});
};
users.editGet = function(req,res){
	functions.sessions.validate(database.collection('users'),req.cookies.sessionId,function(ans){
		if(ans.usertype=='admin'){			
			uid = req.query.uid;
			database.collection('users').find({uid:uid}).toArray(function(err,db){
				if(err){
					console.error(err);
				}
				else if(db.length==0){
					res.render(config.site.template+'/administration/users',{ title:'Admin',brand:config.site.brand,loggedIn:ans,unAuthorised:0,allUsers:[],userId:false,messages:false});
				}
				else{
					res.render(config.site.template+'/administration/users',{ title:'Admin',brand:config.site.brand,loggedIn:ans,unAuthorised:0,allUsers:[],userId:db[0],messages:false});
				}		
			});
		}
		else{
			res.render(config.site.template+'/index', { title: 'GET OUT',brand:config.site.brand,loggedIn:ans,unAuthorised:1});	
		}
	});
};
users.editPost = function(req,res){
	functions.sessions.validate(database.collection('users'),req.cookies.sessionId,function(ans){
		if(ans.usertype=='admin'){
			uid = req.body.uid;
			email = req.body.email;
			user = req.body.user;
			usertype = req.body.usertype;
			pass = req.body.pass;
			confirm = req.body.confirm;
			messages=[]
			if(pass!=confirm){
				messages.push("These passwords are not equal.");
			}
			if(usertype===undefined){
				messages.push("Please put a value for the user type.");	
			}
			if(messages.length==0){
				set = {}
				set.email=email;
				set.user=user;
				set.usertype=usertype;
				if(pass!=''){
					set.pass = crypto.createHash('sha1').update(pass).digest("hex");	
				}
				database.collection('users').update({uid:uid},{$set:set});
				res.redirect("/administration/users");
			}
			else{
				database.collection('users').find({uid:uid}).toArray(function(err,db){
					res.render(config.site.template+'/administration/users',{ title:'Admin',brand:config.site.brand,loggedIn:ans,unAuthorised:0,allUsers:[],userId:db[0],messages:messages});
				});	
			}
		}
		else{res.render(config.site.template+'/index', { title: 'GET OUT',brand:config.site.brand,loggedIn:ans,unAuthorised:1});	}
	});
}

app.get('/administration/users',users.get);
app.get('/administration/users/edit',users.editGet);
app.post('/administration/users/edit',users.editPost);
