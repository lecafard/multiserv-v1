
/*
 * GET home page.
 */

exports.get = function(req, res){
	var users = database.collection('users');
	users.find({activateid:req.query.token}).toArray(function(err, results) {
		if(results.length==0 || results[0].activate==false){
			res.end('The activation id you provided was incorrect.');		
		}
		else{
			users.update({activateid:req.query.token},{$set:{activate:false,disabled:false,activateid:functions.random(50)}})
			res.end('Thanks you activated your account.');
		}
      	});

};
