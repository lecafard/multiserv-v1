
/*
 * GET home page.
 */

exports.get = function(req, res){
  functions.sessions.validate(database.collection('users'),req.cookies.sessionId,function(loggedIn){
	if(loggedIn==false){
  		res.render(config.site.template+'/register', { title: 'Register', messages:'',brand:config.site.brand,email:'',user:'',loggedIn:loggedIn});
	}
	else{
		res.redirect('/');
	}
  });
};
exports.post = function(req,res){
	functions.sessions.register.go(database.collection('users'),req,function(messages){
		res.render(config.site.template+'/register', { title: 'Register', messages:messages,brand:config.site.brand,email:email,user:user,loggedIn:false});
	});
	
}
