
/*
 * GET home page.
 */

exports.get = function(req, res){
	res.json(500,{status:'err', type:'error'})
};
exports.post = function(req,res){
	page = req.body.page;
	functions.sessions.validate(database.collection('users'),req.cookies.sessionId,function(ans){
		switch(page)
		{
			case "login":
				res.render(config.site.template+'/login', { title: config.site.brand+' - Login',messages:true,ajax:1},function(err,string){
					res.json(200,{status:'ok',page:string});				
				});
				break;
			case "home":
				res.render(config.site.template+'/index', { title: 'Hello',brand:config.site.brand,loggedIn:ans,unAuthorised:0,ajax:1},function(err,string){
					res.json(200,{status:'ok',page:string});				
				});
				break;
			case "register":
				res.render(config.site.template+'/register', { title: 'Register', messages:'',email:'',user:'',ajax:1},function(err,string){
					res.json(200,{status:'ok',page:string});				
				});
			default:
				res.json(200,{status:'ok', type:'404'});
				break;
		}
	});
}
