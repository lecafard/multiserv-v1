	
/*
 * GET home page.
 */

exports.get = function(req, res){
	res.json(500,{ status:'err',type:'error' });
};
exports.post = function(req,res){
	action = req.body.action;
	switch(action)
	{
		case('login'):
			email = req.body.email;
			pass= req.body.pass;
			if(email==undefined||pass==undefined){
				res.json(200,{status:'err',login:'fail'});
			}
			functions.sessions.authenticate(database.collection('users'),email,pass,function(sessId){
				if(sessId==false){
					res.json(200,{status:'ok',login:'fail'});
				}
				else{
					res.cookie('sessionId', sessId, { httpOnly: true });
					res.json(200,{status:'ok',login:'pass',redirect:"/"});
				}
			});
			break;
		case('register'):
			switch(req.body.type)
			{
				case('checkAvailability'):
					functions.sessions.register.checkAvailability(database.collection('users'),req,function(result){
						res.json(200,{status:'ok', type:'register',availability:result});			
					});
					break;
				case('go'):
					functions.sessions.register.go(database.collection('users'),req,function(err){
						res.json(200,{status:'ok',type:'register',errors:err});			
					});
					break;
			break;
		}
	}
}
