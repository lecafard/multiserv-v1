
/*
 * GET home page.
 */

exports.get = function(req, res){
  functions.sessions.validate(database.collection('users'),req.cookies.sessionId,function(ans){
		res.render(config.site.template+'/index', { title: 'Hello',brand:config.site.brand,loggedIn:ans,unAuthorised:0});
  });
  
};
