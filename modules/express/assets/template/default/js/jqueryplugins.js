(function ($) {

	$.extend({
	
		gracefulWebSocket: function (url, options) {
		
			// Default properties 
			this.defaults = {
				keepAlive: false,		// not implemented - should ping server to keep socket open
				autoReconnect: false,	// not implemented - should try to reconnect silently if socket is closed
				fallback: true,			// not implemented - always use HTTP fallback if native browser support is missing
				fallbackSendURL: url.replace('ws:', 'http:').replace('wss:', 'https:'),
				fallbackSendMethod: 'POST',
				fallbackPollURL: url.replace('ws:', 'http:').replace('wss:', 'https:'),
				fallbackPollMethod: 'GET',
				fallbackOpenDelay: 100,	// number of ms to delay simulated open event
				fallbackPollInterval: 3000,	// number of ms between poll requests
				fallbackPollParams: {}		// optional params to pass with poll requests
			};
			
			// Override defaults with user properties
			var opts = $.extend({}, this.defaults, options);
			
			/**
			 * Creates a fallback object implementing the WebSocket interface
			 */
			function FallbackSocket() {
				
				// WebSocket interface constants
				const CONNECTING = 0;
				const OPEN = 1;
				const CLOSING = 2;
				const CLOSED = 3;
				
				var pollInterval;
				var openTimout;
				
				// create WebSocket object
				var fws = {
					// ready state
					readyState: CONNECTING,
					bufferedAmount: 0,
					send: function (data) {
						var success = true;
						$.ajax({
							async: false, // send synchronously
							type: opts.fallbackSendMethod,
							url: opts.fallbackSendURL + '?' + $.param( getFallbackParams() ),
							data: data,
							dataType: 'text',
							contentType : "application/x-www-form-urlencoded; charset=utf-8",
							success: pollSuccess,
							error: function (xhr) {
								success = false;
								$(fws).triggerHandler('error');
							}
						});
						return success;
					},
					close: function () {
						clearTimeout(openTimout);
						clearInterval(pollInterval);
						this.readyState = CLOSED;
						$(fws).triggerHandler('close');
					},
					onopen: function () {},
					onmessage: function () {},
					onerror: function () {},
					onclose: function () {},
					previousRequest: null,
					currentRequest: null
				};
				
				function getFallbackParams() {
					
					// update timestamp of previous and current poll request
					fws.previousRequest = fws.currentRequest;					
					fws.currentRequest = new Date().getTime();
					
					// extend default params with plugin options
					return $.extend({"previousRequest": fws.previousRequest, "currentRequest": fws.currentRequest}, opts.fallbackPollParams);
				}
				
				/**
				 * @param {Object} data
				 */
				function pollSuccess(data) {
					
					// trigger onmessage
					var messageEvent = {"data" : data};
					fws.onmessage(messageEvent);	
				}
				
				function poll() {
					
					$.ajax({
						type: opts.fallbackPollMethod,
						url: opts.fallbackPollURL,
						dataType: 'text',
						data: getFallbackParams(),
						success: pollSuccess,
						error: function (xhr) {			
							$(fws).triggerHandler('error');
						}
					});		
				}
				
				// simulate open event and start polling
				openTimout = setTimeout(function () { 
					fws.readyState = OPEN;
					//fws.currentRequest = new Date().getTime();
					$(fws).triggerHandler('open');
					poll();
					pollInterval = setInterval(poll, opts.fallbackPollInterval);
					
				}, opts.fallbackOpenDelay);
				
				// return socket impl
				return fws;
			}
			
			// create a new websocket or fallback
			var ws = window.WebSocket ? new WebSocket(url) : new FallbackSocket();
	 		$(window).unload(function () { ws.close(); ws = null });
			return ws;
		}
	});
	
})(jQuery);
$.fn.onEnterKey =
    function( closure ) {
        $(this).keypress(
            function( event ) {
                code = event.keyCode ? event.keyCode : event.which;

                if ( code == 13 ) {
                    closure();

                    return false;
                }
            } );
}

(function($) {

	$.fn.pinned = function(options,pinning,unpinned) {
		var defaults = {
			bounds: '0px',
			scrolling: '0px',
			mobile: false
		}

		if(pinning && unpinned){
			var callback = {
				pinning: pinning,
				unpinned: unpinned
			}
		}

		return this.each(function() {

			var settings = $.extend(defaults,options);
			var callbacks = $.extend(callback,pinning,unpinned);

			var $this = $(this);
			var orig = $this.css('top');
			$this.data('pinned',true);

			var pinnedTimeout = 0;

			function init(){
				if(isMobile() && settings.mobile == false || !isMobile() ){
					windowScroll();
				}
				else{
					mobileScroll();
				}
			}

			windowScroll = function(){
				if($this.data('pinned'))
					$(window).scroll(function(){
						if ($(window).scrollTop() > settings.bounds && $this.css('position') != 'fixed' ){ 
							$this.css({'position': 'fixed', 'top': settings.scrolling});
							if(callbacks.pinning != null){
								callbacks.pinning();
							}
						} 
						if ($(window).scrollTop() < settings.bounds && $this.css('position') != 'absolute'){ 
							$this.css({'position': 'absolute', 'top': orig}); 
							if(callbacks.unpinned != null){
								callbacks.unpinned();
							}
						}
					});
			}

			// TODO - Add better support for mobile devices on scroll
			mobileScroll = function(){
				if($this.data('pinned',true))
					$(window).bind('touchmove',function(){
						if ($(window).scrollTop() > settings.bounds && $this.css('position') != 'fixed' ){ 
							$this.css({'position': 'fixed', 'top': settings.scrolling});

						} 
						if ($(window).scrollTop() < settings.bounds && $this.css('position') != 'absolute'){ 
							$this.css({'position': 'absolute', 'top': orig}); 

						}
					});
			}

			isMobile = function(){
				if((navigator.userAgent.match(/iPhone/i)) || (navigator.userAgent.match(/iPad/i))) {
					return true;
				}
				else {
					return false;
				}
			}
			init();
    });
	};

})(jQuery);
