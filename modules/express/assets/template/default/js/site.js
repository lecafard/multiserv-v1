$(document).ready(function () {
    $('.alert').alert()
});

$("#nav_login").click(function () {
    $("#main").animate({
        height: "hide",
        width: "hide"
    }, 1000, "swing")
    setTimeout(function () {
        $.post("ajax/reqpage", {
            page: 'login'
        }, function (data) {
            $("#main").html(data.page);
            $("#main").animate({
                height: "show",
                width: "show"
            }, 1000, "swing");
        });
    }, 1000);
    return false
});
$("#nav_home").click(function () {
    $("#main").animate({
        height: "hide",
        width: "hide"
    }, 1000, "swing")
    setTimeout(function () {
        $.post("ajax/reqpage", {
            page: 'home'
        }, function (data) {
            $("#main").html(data.page);
            $("#main").animate({
                height: "show",
                width: "show"
            }, 1000, "swing");
        });
    }, 1000);
    return false
});
$("#nav_register").click(function () {
    $("#main").animate({
        height: "hide",
        width: "hide"
    }, 1000, "swing")
    setTimeout(function () {
        $.post("ajax/reqpage", {
            page: 'register'
        }, function (data) {
            $("#main").html(data.page);
            $("#main").animate({
                height: "show",
                width: "show"
            }, 1000, "swing");
        });
    }, 1000);
    return false
});
