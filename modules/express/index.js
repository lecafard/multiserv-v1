var http = require('http');
var express = require('express');
var ejslayouts = require('express-ejs-layouts');
var ejs = require("ejs");
GLOBAL.app = express();
// all environments
app.set('port', config.port);
app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');
app.use(express.logger());
app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(express.cookieParser());
app.use(app.router);
app.use(express.compress());
app.use(ejslayouts);

app.locals.cdn = config.site.cdn;
app.use('/templates', express.static(__dirname + '/templates'));
app.use('/assets', express.static(__dirname + '/assets'));
// development only
require('./paths');
app.use(express.errorHandler());
app.use(express.favicon(__dirname + '/favicon.ico')); 
GLOBAL.server = http.createServer(app).listen(app.get('port'), function(){
  console.log('The server is listening on port ' + app.get('port'));
});
var path           = require('path')
  , templatesDir   = path.join(__dirname, 'views/default')
  , emailTemplates = require('email-templates');


