//CONTROLLERS OF PATHS
require('./routes/administration');
require('./routes/ajax');

var root = require('./routes/root');
app.get('/', root.get);

var login = require('./routes/login');
app.get('/login',login.get);
app.post('/login',login.post);

var logout = require('./routes/logout');
app.get('/logout',logout.get);

var register = require('./routes/register');
app.get('/register',register.get);
app.post('/register',register.post);

var activateacc = require('./routes/activateacc');
app.get('/activateacc',activateacc.get);

var chat = require('./routes/chat');
app.get('/chat',chat.get);

var helpline = require("./routes/helpline");
app.get("/helpline",helpline.get);
app.post("/helpline",helpline.post);


