var smtp = require('nodemailer')
var path           = require('path')
  , templatesDir   = path.join(__dirname, 'templates/'+config.mail.template)
  , emailTemplates = require('email-templates');
var smtpserver = smtp.createTransport('SMTP',config.mail.server);
sendmail = function(mailOptions){
smtpserver.sendMail(mailOptions, function(error, response){
    if(error){
        console.log(error);
    }else{
        console.log("Message sent: " + response.message);
    }
      
});
}

GLOBAL.emailSender = {}
emailSender.send = {}
emailSender.send.register = function(locals){
emailTemplates(templatesDir, function(err, template) {

  // Render a single email with one template
  
  template('register', locals, function(err, html, text) {
    if(err){
	console.log("Mail Sender: " + err);
    }
    else{
	options = {
          from: config.site.email,
          to: locals.email,
          subject: 'Please Activate Your Account - '+config.site.brand,
          html: html
        }
	sendmail(options);
}
  });
});
}

