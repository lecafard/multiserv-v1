
exports.validate=function(table,cookie,callback){
			find=table.find({sessionId:cookie})
			find.toArray(function(err,results){
				if(err){
					console.error(err)				
				}
				else if (results.length==1){
					
					callback(results[0]);
				}
				else{
					callback(false)		
				}			
			})

};
exports.authenticate = function(table,email,pass,callback){
	hash = crypto.createHash('sha1').update(pass).digest("hex");
	table.find({email:email,pass:hash,activate:false}).toArray(function(err,result){
		if(err){throw(err)}
		else if (result.length==1){
				sessionId = functions.random(60);
				table.update(result[0],{$push:{sessionId:sessionId}})
				callback(sessionId);					
		}
		else{
			callback(false);	
		}
	});
};
exports.register = {}
exports.register.checkAvailability=function(table,req,callback){
		email = req.body.email;
		user = req.body.user;
		var validator = new require('validator').check()
		validator.error = function(msg) {
	    		return false;
		}
		if(validator.check(email).isEmail()!=false&&email!=''){
			table.find({email:email}).toArray(function(err,n){
				if(n.length>0){
					callback(false);			
				}
				else if(n.length==0){
					callback(true);				
				}
			});	
		}
		else if(user!=''|| user!=undefined){
			if(validator.check(user).len(3,64)!=false){
				table.find({user:user}).toArray(function(err,n){
					if(n.length>0){
						callback(false);			
					}
					else if(n.length==0){
						callback(true);				
					}
				});
			}
			else{
				callback(false);
				return		
			}
		}
		else{
			callback(false);		
		}

};
exports.register.go=function(table,req,callback){
		email = req.body.email;
		user = req.body.user;
		pass = req.body.pass;
		confirm = req.body.confirm;
		var validator = new require('validator').check()
		validator.error = function(msg) {
	    		return false;
		}
		table.find({$or:[{email:email},{user:user}]}).toArray(function(err,n){
			console.log(n);
			var messages = []
			if(n.length>0){
				messages.push('That user is already here. Stop trying to spam me.');			
			}
			if(user==''||validator.check(user).len(3,64)==false){
				messages.push('Your username is too short.');
			}
			if(validator.check(email).isEmail()==false){
				messages.push('Gimme a valid email address.')
			}
			if(user.length>30){
				messages.push("Your username is wayyyyyy too long.")			
			}
			if(pass!=confirm || pass==''){
				messages.push('Supply me with the same passwords or supply me a password that is not empty.');
			}
			if(messages.length==0){
				time = new Date()
				time = time.getTime()
				hash = crypto.createHash('sha1').update(pass).digest("hex");
				activate = functions.random(80);
				uid = functions.random(20);
				table.insert({email:email,user:user,pass:hash,activate:true,activateid:activate,disabled:true,creationtime:time,usertype:'user',sessionId:[],uid:uid});
				url = "http://"+config.site.rooturl+"/activateacc/?token="+activate
				emailSender.send.register({email:email,url:url});
				messages = true;
				user = ""
				email = ""
				callback(true)
			}
			else{
				callback(messages);
			}
	});
};
