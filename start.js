//Server
var clim = require("clim");
clim(console, true);
var cluster = require('cluster');
GLOBAL.functions = require('./functions')
GLOBAL.config = require('./config')
GLOBAL.path = require('path');
GLOBAL.fs = require('fs');
GLOBAL.crypto = require('crypto');
GLOBAL.WebSocket = require('ws').Server;
require('./db');
function startapp(){
require('./modules');
process.on( 'SIGINT', function() {
  console.log( "\nShutting Down Worker Server (Crtl-C)" )
  // some other closing procedures go here
  server.close();
  process.exit()
});
}

if (cluster.isMaster && config.options.cluster.enabled == 1) {
  // Fork workers.
  for (var i = 0; i < config.options.cluster.processes; i++) {
    cluster.fork();
  }
  require('./init');
  cluster.on('exit', function(worker, code, signal) {
    console.log('worker ' + worker.process.pid + ' died');
  });
  process.on( 'SIGINT', function() {
  	console.log( "\nShutting Down Master Server (Crtl-C)" );
	temp = database.collection('temp');
	temp.remove();
	database.close();
	process.exit();
  });
}
else if (config.options.cluster.enabled == 0){
	require('./init');
	startapp()
}
else{
	startapp()
}
