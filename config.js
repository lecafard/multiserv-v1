//CONFIG

exports.mail = {
	server:{
		host:"mail.optusnet.com.au",
		port:25
	},
	template:'default'
};
exports.port = 8080;
exports.mongodb = {
	url:"dbuser:hello@127.0.0.1:27017",
	host:'127.0.0.1',
	port:27017,
	options:{auto_reconnect:true,poolSize:5,safe:true},
	db: 'multipurpose-1',
	user:'dbuser',
	pass:'hello'
};
exports.site = {
	email: "Site Admin <noreply@tweb2.tk>",
	rooturl:'home.tweb2.tk:8080',
	brand:'Random Server',
	template:'default',
	cdn:'/assets/template/'
};
exports.options = {
	cluster: {
		enabled: 1,
		processes: 8
	}
}
exports.mq = {
    port: 61613,
    host: 'localhost',
    debug: false,
    login: 'guest',
    passcode: 'guest',
};
